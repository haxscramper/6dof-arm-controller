#-------------------------------------------------
#
# Project created by QtCreator 2016-09-24T10:20:23
#
#-------------------------------------------------

QT       += core gui opengl serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Final
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    owidget.cpp \
    compute.cpp

HEADERS  += mainwindow.h \
    owidget.h \
    compute.h

FORMS    += mainwindow.ui


LIBS += -lOpengl32\
        -lglut -lGlu32

DISTFILES += \
    Temp \
    COMPUTE EXPLAINED \
    README \
    COMPUTE
