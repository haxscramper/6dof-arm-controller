#ifndef COMPUTE_H
#define COMPUTE_H

float * forward6dof(float len1,float len2,float len3,float a1,float a2,float a3,float aBase);

float * forwardDelta(float a1, float a2, float a3,float len1,float len2,float len3,float len4);

float * delta_calcInverse(float x0, float y0, float z0,float len1 , float len2, float len3,float len4);

float * inverse6dof(float len1,float len2,float len3,float x,float y,float z);

float * inverseScara(float len1,float len2,float x,float y, float z);

float * forwardScara(float len1,float len2,float a1,float a2,float yPos);

float * forward4dof(float len1,float len2,float aBase,float a1,float a2);

double degrees(double radians);

double radians(double degrees);

float * inverse4dof(float len1,float len2,float x,float y,float z);

float * inverseDelta(float x0, float y0, float z0,float len1 , float len2, float len3,float len4);

#endif // COMPUTE_H
