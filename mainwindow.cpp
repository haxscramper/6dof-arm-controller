#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <compute.h>
#include <owidget.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::change6dof(float x,float y,float z){
    if(ui->check_inverse_2->isChecked()){
    float *result;
    result = inverse6dof(len1_6dof,len2_6dof,len3_6dof,x,y,z);
    ui->openGLWidget->changeA1(result[0]);
    ui->openGLWidget->changeA2(result[1]);
    ui->openGLWidget->changeA3(result[2]);
    ui->openGLWidget->changeABase(result[3]);
    }
}
void MainWindow::on_slider_aBase_2_valueChanged(int value)
{
    ui->openGLWidget->changeABase(value);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[0]);
    ui->slider_y_2->setValue(result[1]);
    ui->slider_z_2->setValue(result[2]);

}
void MainWindow::on_slider_a1_2_valueChanged(int value)
{
    ui->openGLWidget->changeA1(value);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[1]);
    ui->slider_y_2->setValue(result[2]);
    ui->slider_z_2->setValue(result[3]);
}
void MainWindow::on_slider_a2_2_valueChanged(int value)
{
    ui->openGLWidget->changeA2(value);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[1]);
    ui->slider_y_2->setValue(result[2]);
    ui->slider_z_2->setValue(result[3]);
}
void MainWindow::on_slider_a3_2_valueChanged(int value)
{
    ui->openGLWidget->changeA3(value);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[1]);
    ui->slider_y_2->setValue(result[2]);
    ui->slider_z_2->setValue(result[3]);
}
void MainWindow::on_slider_medR_2_valueChanged(int value)
{
    ui->openGLWidget->changeMedR(value);
}
void MainWindow::on_slider_lowR_2_valueChanged(int value)
{
    ui->openGLWidget->changeLowR(value);
}
void MainWindow::on_slider_grip_2_valueChanged(int value)
{
    ui->openGLWidget->changeGrip(value);
}
void MainWindow::on_slider_viewX_2_valueChanged(int value)
{
    ui->openGLWidget->changeCamX(value);
}
void MainWindow::on_slider_viewY_2_valueChanged(int value)
{
     ui->openGLWidget->changeCamY(value);
}
void MainWindow::on_slider_viewZ_2_valueChanged(int value)
{
     ui->openGLWidget->changeCamZ(value);
}
void MainWindow::on_camRotX_valueChanged(int value)
{
    ui->openGLWidget->changeCamRotX(value);
}
void MainWindow::on_camRotY_valueChanged(int value)
{
    ui->openGLWidget->changeCamRotY(value);
}
void MainWindow::on_camRotZ_valueChanged(int value)
{
   ui->openGLWidget->changeCamRotZ(value);
}
void MainWindow::update6dof( int len1,int len2,int len3,int len4)
{
    if(ui->check_inverse_2->isChecked())
    {
        float *result;
        result = inverse6dof(len1,len2,len3,xEnd,yEnd,zEnd);
        ui->openGLWidget->changeA1(result[1]);
        ui->openGLWidget->changeA2(result[2]);
        ui->openGLWidget->changeA3(result[3]);
        ui->openGLWidget->changeABase(result[4]);
        ui->openGLWidget->updateRobot(1,len1,len2,len3,len4);
    }
    else
    {
        ui->openGLWidget->updateRobot(1,len1,len2,len3,len4);
    }
}
void MainWindow::on_spinBox_len1_6dof_valueChanged(int arg1)
{
    len1_6dof=arg1;
    ui->openGLWidget->updateRobot(1,len1_6dof,len2_6dof,len3_6dof,len4_6dof);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[1]);
    ui->slider_y_2->setValue(result[2]);
    ui->slider_z_2->setValue(result[3]);
}
void MainWindow::on_spinBox_len2_6dfof_valueChanged(int arg1)
{
    len2_6dof=arg1;
     ui->openGLWidget->updateRobot(1,len1_6dof,len2_6dof,len3_6dof,len4_6dof);
     float *result;
     result = ui->openGLWidget->getValues();
     ui->slider_x_2->setValue(result[1]);
     ui->slider_y_2->setValue(result[2]);
     ui->slider_z_2->setValue(result[3]);
}
void MainWindow::on_spinBox_len3_6dof_valueChanged(int arg1)
{
    len3_6dof=arg1;
    ui->openGLWidget->updateRobot(1,len1_6dof,len2_6dof,len3_6dof,len4_6dof);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[1]);
    ui->slider_y_2->setValue(result[2]);
    ui->slider_z_2->setValue(result[3]);
}
void MainWindow::on_spinBox_len1_4dof_valueChanged(int arg1)
{
    len1_4dof=arg1;
}
void MainWindow::on_spinBox_len2_4dof_valueChanged(int arg1)
{
    len2_4dof=arg1;
}
void MainWindow::on_spinBox_len1_scara_valueChanged(int arg1)
{
    len1_scara=arg1;
}
void MainWindow::on_spinBox_len2_scara_valueChanged(int arg1)
{
    len2_scara=arg1;
}
void MainWindow::on_spinBox_lenBaseTop_delta_valueChanged(int arg1)
{
    len1_delta=arg1;
}
void MainWindow::on_spinBox_lenBaseBottom_delta_valueChanged(int arg1)
{
    len2_delta=arg1;
}
void MainWindow::on_spinBox_lenJointTop_delta_valueChanged(int arg1)
{
    len3_delta=arg1;
}
void MainWindow::on_spinBox_lenJointBottom_Delta_valueChanged(int arg1)
{
    len4_delta=arg1;
}
void MainWindow::on_spinBox_len4_6dof_valueChanged(int arg1)
{
    len4_6dof=arg1;
    ui->openGLWidget->updateRobot(1,len1_6dof,len2_6dof,len3_6dof,len4_6dof);
    float *result;
    result = ui->openGLWidget->getValues();
    ui->slider_x_2->setValue(result[1]);
    ui->slider_y_2->setValue(result[2]);
    ui->slider_z_2->setValue(result[3]);
}
void MainWindow::setXEnd(float val)
{
    ui->slider_x_2->setValue((int)val);
}
void MainWindow::setYEnd(float val)
{
    ui->slider_y_2->setValue((int)val);
}
void MainWindow::setZEnd(float val)
{
    ui->slider_z_2->setValue((int)val);
}
void MainWindow::on_slider_x_2_sliderMoved(int position)
{
    change6dof(position,ui->slider_y_2->value(),ui->slider_z_2->value());
}
void MainWindow::on_slider_y_2_sliderMoved(int position)
{
    change6dof(ui->slider_x_2->value(),position,ui->slider_z_2->value());
}
void MainWindow::on_slider_z_2_sliderMoved(int position)
{
    change6dof(ui->slider_x_2->value(),ui->slider_y_2->value(),position);
}
